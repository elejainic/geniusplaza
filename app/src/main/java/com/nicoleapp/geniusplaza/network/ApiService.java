package com.nicoleapp.geniusplaza.network;

import com.nicoleapp.geniusplaza.network.model.Datum;
import com.nicoleapp.geniusplaza.network.model.UserList;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiService {


    // Create note
    @POST("users")
    Single<Datum> createUser(@Body Datum user);

    // Fetch all notes
    @GET("users")
    Single<UserList> fetchAllUsers();

}