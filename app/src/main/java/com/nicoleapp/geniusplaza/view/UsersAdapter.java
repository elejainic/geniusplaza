package com.nicoleapp.geniusplaza.view;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nicoleapp.geniusplaza.R;
import com.nicoleapp.geniusplaza.network.model.Datum;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.MyViewHolder> {

    private Context context;
    private List<Datum> usersList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.lastName)
        TextView lastNmae;

        @BindView(R.id.image)
        ImageView image;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public UsersAdapter(Context context, List<Datum> usersList) {
        this.context = context;
        this.usersList = usersList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Datum user = usersList.get(position);

        String imageUrl = user.getAvatar();

        holder.name.setText(user.getFirstName());

        holder.lastNmae.setText(user.getLastName());

        Picasso.get().load(imageUrl).fit().centerInside().into(holder.image);

        //holder.image.setText(user.getAvatar());


    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }


}
